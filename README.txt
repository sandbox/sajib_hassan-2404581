Description
===========

In general, Allows the admin to select which content types, taxonomy, access 
role to restrict node page view access for specific node-types and redirect to 
404 as well as from crawlers and bot for this site. Currently there are two 
features implemented.

Restrict node view by Content Types and Taxonomy Vocabularies
-----------------------------------------------------------
When a user is restricted, that user will not be able to view for selected user
 roles.

Restrict data by Robot.txt for bot/crawler
---------------------------------------------
When a Content Type and Taxonomy Vocabulary are restricted, that Content Type 
and Taxonomy Vocabulary will not be available to crawl by the any bot/crawler. 
Robot.txt will disallow it. To enable this feature, we must enable robot.txt 
drupal module [https://www.drupal.org/project/robotstxt].

Note: You must delete or rename the robots.txt file in the root of your Drupal 
installation for this module to display its own robots.txt file(s).

### Install

1. Follow the instructions at
   https://drupal.org/documentation/install/modules-themes/modules-7.


### Usage

1. All configuration can be managed from administration pages located at
   Administration > Configuration > Content authoring > Restrict Node Page View
 404 Settings.
2. Checked individual node/content types for node view restriction, fieldset 
located on the "Restrict Node types".
3. Checked individual taxonomy vocabularies for node view restriction, fieldset
 located on the "Restrict Taxonomy Vocabulary".
4. Checked individual roles for node view restriction will apply, fieldset 
located on the "Restrict Role Users to view Page".
5. Select a page whare restricted node view will redirect, fieldset located 
on the "Redirect To". Currently there are three options - Home Page, Page not 
Found, Page access denied.
6. Select "Yes" if we want to restrict above selected "Content Types and 
Taxonomy Vocabularies" from bot/crawler, fieldset located on the "Disallow to 
robots.txt". To use this feature, we must enable robot.txt drupal module 
[https://www.drupal.org/project/robotstxt].
7. To remove, simply un-checked its value, and submit the configuration form.

### Authors

Please see project page for current list of maintainers.

* Sajib Hassan (sajib.hassan [at] gmail.com)
